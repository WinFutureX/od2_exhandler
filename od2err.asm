; exception handler from overdrive 2 (partially disassembled)
; disassembly by kelsey boey
; overdrive 2 demo copyright (c) 2017 titan, i guess...?

; equates section (rom/io area)
version	equ	$A10001			; mega drive hardware revision (byte)
jsctrl1	equ	$A10003			; controller 1 control (byte)
jsdata1	equ	$A10009			; controller 1 data (byte)
vdpctrl	equ	$C00004			; vdp control port
vdpdata	equ	$C00000			; vdp data port
hvcount	equ	$C00008			; h/v counter

; equates section (ram)
rambgn	equ	$FF0000			; start of ram
v_unk0	equ	$FF0400			; unknown variable 0 (long)
v_unk1	equ	$FF0404			; unknown variable 1 (word)
v_unk2	equ	$FF0406			; unknown variable 2 (byte)
v_unk3	equ	$FF0407			; unknown variable 3 (byte)
v_unk4	equ	$FF0408			; unknown variable 4 (long)
v_unk5	equ	$FF040C			; unknown variable 5 (long)
v_unk6	equ	$FF0410			; unknown variable 6 (word)
vbiloc	equ	$FF0412			; vblank vector pointer (long)
vbiend	equ	$FF0416			; vblank finished flag (byte)
v_unk7	equ	$FF0417			; unknown variable 7 (byte)
v_unk8	equ	$FF0418			; unknown variable 8 (long)
v_unk9	equ	$FF041C			; unknown variable 9 (long, unused?)
hbiloc	equ	$FF069A			; hblank vector pointer (long)
v_unka	equ	$FF079A			; unknown variable 10 (long)

; carbon copy of header
vectors:
	dc.l	$1000000
	dc.l	startup_68k
	rept	26
	dc.l	cpufault
	endr
	dc.l	hbiloc
	dc.l	cpufault
	dc.l	vblank
	rept	33
	dc.l	cpufault
	endr

mdheader:
	dc.b	"SEGA SSF        "
	dc.b	"(C)TITAN2017.APR"
	dc.b	"OVERDRIVE 2                                     "
	dc.b	"OVERDRIVE 2                                     "
	dc.b	"DE TITAN002-XX"
	dc.w	0
	dc.b	"J               "
	dc.l	$0
	dc.l	$3FFFFF
	dc.l	$FF0000
	dc.l	$FFFFFF
	dc.l	$20202020
	dc.l	$20202020
	dc.l	$20202020
	dc.b	"                                                    "
	dc.b	"E               "

; code section
resetvideo:
loc200:
	move.w	#$2700, sr
	lea	$C00004, a0
	move.w	#0, $18(a0)
	move.w	#$8014, (a0)
	move.w	#$8134, (a0)
	move.w	#$8230, (a0)
	move.w	#$8334, (a0)
	move.w	#$8407, (a0)
	move.w	#$8578, (a0)
	move.w	#$8700, (a0)
	move.w	#$8AFF, (a0)
	move.w	#$8B00, (a0)
	move.w	#$8C81, (a0)
	move.w	#$8D3E, (a0)
	move.w	#$9001, (a0)
	move.w	#$9100, (a0)
	move.w	#$9200, (a0)
	move.w	#$8F02, (a0)
	rts

setupsystem:
loc24e:
	moveq	#0, d7
	move.w	#$2700, sr
	move.w	d7, d0
	lea	$C00004, a0
	btst.l	#8, d0
	bne.s	loc268
	move.w	#0, $18(a0)

loc268:
	btst.l	#7, d0
	bne.s	loc2a2

; vdp setup
loc26e:
	move.w	#$8014, (a0)
	move.w	#$8134, (a0)
	move.w	#$8230, (a0)
	move.w	#$8334, (a0)
	move.w	#$8407, (a0)
	move.w	#$8578, (a0)
	move.w	#$8700, (a0)
	move.w	#$8AFF, (a0)
	move.w	#$8C81, (a0)
	move.w	#$8D3E, (a0)
	move.w	#$9001, (a0)
	move.w	#$9100, (a0)
	move.w	#$9200, (a0)

loc2a2:
	move.w	#$8F02, (a0)		; vdp increment (2 bytes)
	btst.l	#0, d0
	bne.s	loc2b0

loc2ac:
	jsr	loc310.w

loc2b0:
	btst.l	#1, d0
	bne.s	loc2ba
	jsr	loc318.w

loc2ba:
	btst.l	#2, d0
	bne.s	loc2c4
	jsr	loc320.w

loc2c4:
	btst.l	#3, d0
	bne.s	loc2ce
	jsr	loc3ec.w

loc2ce:
	btst.l	#4, d0
	bne.s	loc2d8
	jsr	loc358.w

loc2d8:
	btst.l	#5, d0
	bne.s	loc2e2
	jsr	loc37a.w

loc2e2:
	btst.l	#6, d0
	bne.s	loc2ec
	jsr	loc39c.w

loc2ec:
	btst.l	#9, d0
	bne.s	loc2f6
	jsr	loc454.w

loc2f6:
	btst.l	#$A, d0
	bne.s	loc30a
	clr.l	$FF0412			; clear vblank vector pointer
	move.w	#$4E73, $FF069A		; if hblank vector is in ram ($4E73 = rte)

; *****************************************************************************
; * WARNING FOR THIS SECTION (loc30a):                                        *
; *****************************************************************************
; * this was taken from the original od2 disassembly without modification     *
; * and assumes that your rom has a dynamic hblank vector (i.e. in ram) so    *
; * replace $FF069A with the address of your dynamic hblank routine if needed *
; * and if you don't, replace:                                                *
; * 	move.w	#$2000, sr		; enable interrupts                   *
; * with:                                                                     *
; * 	move.w	#$2700, sr		; disable interrupts                  *
; * to avoid hblank interrupts from interfering with exception handling.      *
; *****************************************************************************
loc30a:
	move.w	#$2000, sr		; enable interrupts
	rts

loc310:
	move.w	#$C000, d7
	jmp	loc328.w

loc318:
	move.w	#$E000, d7
	jmp	loc328.w

loc320:
	move.w	#$D000, d7
	jmp	loc328.w

loc328:
	lea	$C00000, a6
	andi.l	#$FFFE, d7
	add.l	d7, d7
	add.l	d7, d7
	lsr.w	#2, d7
	ori.w	#$4000, d7
	swap	d7
	move.l	d7, $4(a6)
	moveq	#0, d7
	move.w	#$FF, d6

loc34a:
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	dbf	d6, loc34a
	rts

; clear vsram
clearvsram:
loc358:
	lea	$C00000, a6
	moveq	#0, d7
	move.l	#$78000003, $4(a6)	; vsram write @ $0
	move.w	#$3F, d6

vsramloop:
loc36c:
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	dbf	d6, loc36c
	rts

; clear vram
clearvram:
loc37a:
	lea	$C00000, a6
	moveq	#0, d7
	move.l	#$40000010, $4(a6)	; vram write @ $0
	move.w	#4, d6

vramloop:
loc38e:
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	dbf	d6, loc38e
	rts

; clear cram
clearcram:
loc39c:
	lea	$C00000, a6
	moveq	#0, d7
	move.l	#$C0000000, $4(a6)	; cram write @ $0
	move.w	#7, d6

cramloop:
loc3b0:
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	move.l	d7, (a6)
	dbf	d6, loc3b0
	rts

; od2 vblank handler
vblank:
loc3be:
	tst.l	$FF0412			; test vblank vector
	beq.s	loc3d2			; if not set, branch
	move.l	a6, -(a7)		; otherwise, save a6...
	movea.l	$FF0412, a6		; ...and then put vblank vector in a6
	jsr	(a6)			; jump there
	movea.l	(a7)+, a6		; after return, restore old a6

vblank_end:
loc3d2:
	st	$FF0416			; set vblank finished flag?
	rte				; exit vblank

clearflag_vbiend:
loc3da:
	move.b	#0, $FF0416		; clear vblank finished flag?

checkflag_vbiend:
loc3e2:
	tst.b	$FF0416			; check if vblank finished?
	beq.s	loc3e2			; if not, branch
	rts

clearvars:
loc3ec:
	moveq	#0, d7
	move.b	d7, $FF0417		; clear unknown 7
	move.l	d7, $FF0418		; clear unknown 8
	move.l	d7, $FF041C		; clear unknown 9
	rts

loc402:
	lea	$FF0418, a6		; get unknown 8 (pointer?)
	moveq	#0, d7
	move.b	$FF0417, d7		; get unknown 7
	beq.s	loc424
	cmpi.b	#$50, d7
	beq.s	loc438
	lsl.w	#3, d7
	lea	(a6, d7.w), a6
	lsr.w	#3, d7
	move.b	d7, -$5(a6)

loc424:
	move.w	d1, (a6)+
	move.b	d3, (a6)+
	move.b	#0, (a6)+
	move.w	d2, (a6)+
	move.w	d0, (a6)+
	addq.b	#1, d7
	move.b	d7, $FF0417

loc438:
	rts

loc43a:
	lea	$FF0000, a6
	movea.l	$FF0400, a5
	lea	$C00004, a4

loc44c:
	cmpa.l	a6, a5
	beq.s	loc454
	move.w	(a6)+, (a4)
	bra.s	loc44c

loc454:
	move.l	#$FF0000, $FF0400
	rts

loc460:
	movem.l	a2-a0, -(sp)
	andi.w	#7, d0
	movem.w	d5-d0, -(sp)
	subq.w	#1, d1
	move.w	#$1110, d5
	add.w	d0, d0
	lea	loc4b4, a2
	move.w	(a2, d0.w), d0

loc47e:
	move.w	(a0), d2
	move.w	(a1)+, d3
	move.w	d2, d4
	or.w	d5, d4
	sub.w	d3, d4
	or.w	d5, d3
	sub.w	d2, d3
	and.w	d0, d3
	and.w	d0, d4
	sub.w	d3, d4
	lsr.w	#3, d4
	sub.w	d4, d2
	andi.w	#$EEE, d2
	move.w	d2, (a0)+
	dbf	d1, loc47e

loc4a0:
	movem.w	(a7)+, d0-d5
	lea	$4C4, a2
	move.b	(a2, d0.w), d0
	movem.l	(sp)+, a0-a2
	rts

loc4b4:
	ori.b	#0, d0
	btst.l	d0, d0
	move.b	d0, -(a0)
	ori.b	#$10, (a0)
	btst.b	d0, (a0)
	move.b	(a0), -(a0)
	ori.b	#5, d4
	andi.b	#7, d6

loc4cc:
	move.b	#$40, $A10009
	move.b	#$40, $A10003
	clr.l	$FF0408
	clr.l	$FF040C
	clr.w	$FF0410
	rts

; get controller input? (d7 = pressed buttons, a6 & d6 trashed)
getinput_joy1:
loc4f0:
	lea	$A10003, a6
	move.b	#$40, (a6)		; set TH pin = 1
	nop
	nop
	move.b	(a6), d7
	move.b	#0, (a6)		; set TH pin = 0
	nop
	nop
	move.b	(a6), d6
	andi.b	#$30, d6
	andi.b	#$3F, d7
	add.b	d6, d6
	add.b	d6, d6
	or.b	d6, d7
	not.b	d7
	move.b	$FF0406, d6
	move.b	d7, $FF0406
	not.b	d6
	and.b	d6, d7
	move.b	d7, $FF0407
	tst.b	d7
	beq.s	loc548
	lea	$FF0408, a6
	moveq	#8, d6

loc53c:
	move.b	$1(a6), (a6)
	addq.w	#1, a6
	dbf	d6, loc53c
	move.b	d7, (a6)		; d7 = SACBRLDU

loc548:
	rts

loc54a:
	lea	$FF0408, a6
	;dc.l	$4BFA0026		; hack
	lea	loc578(pc), a5
	moveq	#9, d7

loc556:
	cmpm.b	(a6)+, (a5)+
	bne.s	loc574
	dbf	d7, loc556
	clr.l	$FF0408
	clr.l	$FF040C
	clr.w	$FF0410
	moveq	#1, d7
	rts

loc574:
	moveq	#0, d7
	rts

dataloc0:
loc578:	; is this data? (invalid instructions happen after this)
	;btst.l	d0, d1 (POSSIBLY INVALID)
	dc.w	$0101
	;andi.b	#8, d2 (POSSIBLY INVALID)
	dc.w	$0202
	dc.w	$0408
	; INVALID
	dc.w	$0408			; loc57e
	; INVALID
	dc.w	$1040			; loc580

loc582:	; valid(?) code continues
	move.w	d3, -(sp)
	move.w	d4, -(sp)
	move.b	(a6)+, d7
	lsl.w	#8, d7
	move.b	(a6)+, d7
	moveq	#1, d6

loc58e:
	tst.w	d7
	beq.w	loc616
	subq.w	#1, d6
	bne.s	loc59c
	move.b	(a6)+, d5
	moveq	#8, d6

loc59c:
	add.b	d5, d5
	bcc.s	loc60e
	move.b	(a6)+, d3
	lsl.w	#8, d3
	move.b	(a6)+, d3
	move.b	d3, d4
	lsr.w	#4, d3
	andi.w	#$F, d4
	subq.w	#3, d7
	sub.w	d4, d7
	addq.w	#3, d3
	neg.w	d3
	add.w	d4, d4
	add.w	d4, d4
	eori.w	#$3C, d4
	;jmp	*+$5C2(pc, d4.w)
	;dc.l	$4EFB4002		; hack
	jmp	loc5c2(pc, d4.w)

loc5c2:
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	move.b	(a5, d3.w), (a5)+
	bra.w	loc58e

loc60e:
	move.b	(a6)+, (a5)+
	subq.w	#1, d7
	bra.w	loc58e

loc616:
	move.w	(a7)+, d4
	move.w	(a7)+, d3
	rts

loc61c:
	movem.l	d2/d1/d0, -(sp)
	lsl.w	#3, d7
	subq.w	#1, d7

loc624:
	move.b	(a6)+, d0
	moveq	#7, d2

loc628:
	lsl.l	#4, d1
	add.b	d0, d0
	bcc.s	loc632
	or.b	d6, d1
	bra.s	loc634

loc632:
	or.b	d5, d1

loc634:
	dbf	d2, loc628

loc638:
	move.l	d1, (a5)+
	dbf	d7, loc624
	movem.l	(sp)+, d0-d2
	rts

loc644:
	add.w	$C00008, d7
	lea	$FF0124, a0
	lea	$FF0032, a1
	lea	$FF0330, a2
	lea	$FF0288, a3
	moveq	#$3F, d6

loc664:
	add.w	(a0), d7
	sub.w	(a1), d7
	add.w	(a2), d7
	sub.w	(a3), d7
	lea	$400(a0), a0
	lea	$400(a1), a1
	lea	$400(a2), a2
	lea	$400(a3), a3
	dbf	d6, loc664
	sub.w	$C00008, d7
	rts

loc688:
	move.w	$FF0404, d7
	ror.w	#5, d7
	add.w	$C00008, d7
	rol.w	#2, d7
	sub.w	$C00008, d7
	move.w	d7, $FF0404
	rts

loc6a6:
	bsr.w	loc688
	swap	d7
	bsr.s	loc688
	rts

; this is the 68k entry point
startup_68k:
loc6b0:
	move.w	#$2700, sr
	move.b	$A10001, d7
	andi.b	#$0F, d7
	beq.s	loc6c8
	move.l	$100.w, $A14000

loc6c8:
	jsr	loc644.w
	jsr	loc4cc.w
	jsr	loc24e.w
	bra.w	loc388a			; this is outside the exception handler...
	jsr	loc3da.w
	jsr	loc43a.w
	jsr	loc4f0.w
	jsr	loc54a.w
	tst.w	d7
	bne.w	loc388a			; ...so it may be where the demo starts
	rts

; this is the beginning of the exception handler routine
cpufault:
loc6f0:
	movem.l	a6-d0, -(sp)		; save registers
	move.w	#$2700, sr		; disable interrupts
	jsr	loc24e.w
	lea	$C00004, a0
	lea	$C00000, a1
	move.l	#$40000000, (a0)
	moveq	#0, d7
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	move.l	d7, (a1)
	lea	incdat0, a6		; originally "lea $CCC, a6"
	lea	$FF079A, a5
	moveq	#$24, d7
	moveq	#1, d6
	moveq	#0, d5
	jsr	loc61c.w
	lea	$FF079A, a6
	moveq	#$23, d7

; draw to screen
writereg:
loc73e:
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	move.l	(a6)+, (a1)
	dbf	d7, loc73e
	move.l	#$C0000000, (a0)	; cram write
	move.l	#$08000888, (a1)
	move.l	#$C0200000, (a0)	; cram write
	move.l	#$08000EEE, (a1)
	move.w	#$8154, (a0)
	move.l	#$70000003, (a0)
	move.l	#0, (a1)
	move.l	#$78000003, (a0)
	move.l	#0, (a1)
	move.l	#$40000010, (a0)	; vram write (plane a)
	move.l	#0, (a1)
	lea	hexstr, a2		; originally "lea $CBC, a2"
	lea	incdat1, a3		; originally "lea $DEC, a3"
	move.l	#$40060003, (a0)
	move.w	#$00D2, d1		; "OD2"
	jsr	locc1a
	move.w	#$B50D, d1		; "BSOD"
	jsr	locc16
	; d0
	move.l	#$41060003, (a0)
	move.b	#$D0, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d1
	move.l	#$41860003, (a0)
	move.b	#$D1, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d2
	move.l	#$42060003, (a0)
	move.b	#$D2, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d3
	move.l	#$42860003, (a0)
	move.b	#$D3, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d4
	move.l	#$43060003, (a0)
	move.b	#$D4, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d5
	move.l	#$43860003, (a0)
	move.b	#$D5, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d6
	move.l	#$44060003, (a0)
	move.b	#$D6, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; d7
	move.l	#$44860003, (a0)
	move.b	#$D7, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a0
	move.l	#$45060003, (a0)
	move.b	#$A0, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a1
	move.l	#$45860003, (a0)
	move.b	#$A1, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a2
	move.l	#$46060003, (a0)
	move.b	#$A2, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a3
	move.l	#$46860003, (a0)
	move.b	#$A3, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a4
	move.l	#$47060003, (a0)
	move.b	#$A4, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a5
	move.l	#$47860003, (a0)
	move.b	#$A5, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a6
	move.l	#$48060003, (a0)
	move.b	#$A6, d1
	jsr	locc20
	move.l	(a7)+, d1
	jsr	locc26
	; a7/sp
	move.l	#$48860003, (a0)
	move.b	#$A7, d1
	jsr	locc20
	move.l	a7, d1
	jsr	locc26
	; stack dump
	; prints up to 5 lines x 7 words on screen
	; stops when either one of the following conditions are fulfilled:
	; 1: stack pointer rolls over to $0
	; 2: all 5 lines x 7 words printed
	move.l	#$49860003, (a0)	; 1st line
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	#$4A060003, (a0)	; 2nd line
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	#$4A860003, (a0)	; 3rd line
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	#$4B060003, (a0)	; 4th line
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	#$4B860003, (a0)	; 5th line
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16
	move.l	a7, d0
	andi.l	#$FFFFFF, d0
	beq.w	locc12
	move.w	(a7)+, d1
	jsr	locc16

haltcpu:
locc12:
	stop	#$2700			; halt cpu

; hex printing routines (d1 = byte(s) to print, swapped)
; print 2 bytes
locc16:
	swap	d1
	bra.s	locc6e

; print 3 bytes
locc1a:
	rol.w	#4, d1
	swap	d1
	bra.s	locc80

; print 1 byte/register name
locc20:
	rol.w	#8, d1
	swap	d1
	bra.s	locc92

; print 4 bytes
locc26:
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)

locc6e:
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)

locc80:
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)

locc92:
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)
	rol.l	#4, d1
	move.w	d1, d0
	andi.w	#$F, d0
	move.b	(a2, d0.w), d0
	move.b	(a3, d0.w), d0
	move.w	d0, (a1)
	move.w	#0, (a1)
	rts

dataloc1:
hexstr:
	dc.b	"0123456789ABCDEF"
incdat0:
	incbin	"od2datablk_final_part1.bin"	; just a blob of data, and that's it
incdat1:
	incbin	"od2datablk_final_part2.bin"

loce4c:
	jmp	locee2

nullpad0:
	rept	36
	dc.l	$FFFFFFFF
	endr

locee2:
	move.w	#$2700, sr

nullpad1:
	rept	2665
	dc.l	$FFFFFFFF
	endr

; this is supposed to start the demo, but it's not included here
loc388a:
	bra.s	*